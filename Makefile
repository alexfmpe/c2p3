H = hello
B = llvm/build
P = llvm/src/lib/Target/P3

all:	ll hello

ll:
	cd $B && make -j 8

hello: touch
	cd $H && make 

touch:
	cd $H && make touch
	
clean:
	rm -f $P/*.inc
