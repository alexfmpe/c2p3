//===-- P3SelectionDAGInfo.cpp - P3 SelectionDAG Info -------------===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
// This file implements the P3SelectionDAGInfo class.
//
//===----------------------------------------------------------------------===//

#include "P3TargetMachine.h"
using namespace llvm;

#define DEBUG_TYPE "p3-selectiondag-info"

P3SelectionDAGInfo::P3SelectionDAGInfo(const DataLayout &DL)
    : TargetSelectionDAGInfo(&DL) {}

P3SelectionDAGInfo::~P3SelectionDAGInfo() {
}
