//===-- P3SelectionDAGInfo.h - P3 SelectionDAG Info -----*- C++ -*-===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
// This file defines the P3 subclass for TargetSelectionDAGInfo.
//
//===----------------------------------------------------------------------===//

#ifndef LLVM_LIB_TARGET_P3_P3SELECTIONDAGINFO_H
#define LLVM_LIB_TARGET_P3_P3SELECTIONDAGINFO_H

#include "llvm/Target/TargetSelectionDAGInfo.h"

namespace llvm {

class P3TargetMachine;

class P3SelectionDAGInfo : public TargetSelectionDAGInfo {
public:
  explicit P3SelectionDAGInfo(const DataLayout &DL);
  ~P3SelectionDAGInfo();
};

}

#endif
