//===-- P3MachineFuctionInfo.cpp - P3 machine function info -------===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//

#include "P3MachineFunctionInfo.h"

using namespace llvm;

void P3MachineFunctionInfo::anchor() { }
