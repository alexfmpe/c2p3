tmp=~/tese
build=$tmp/build
llvm=$tmp/llvm
tools=$llvm/tools
clang=$tools/clang
projects=$llvm/projects
compiler_rt=$projects/compiler-rt
test_suite=$projects/test-suite


mkdir $tmp
svn co http://llvm.org/svn/llvm-project/llvm/trunk $llvm
svn co http://llvm.org/svn/llvm-project/cfe/trunk $clang
svn co http://llvm.org/svn/llvm-project/compiler-rt/trunk $compiler_rt
svn co http://llvm.org/svn/llvm-project/test-suite/trunk $test_suite

mkdir $build 
cd $build &&  ../src/configure --enable-optimized --disable-assertions --enable-target=P3 && make -j 8


